<?php

// Include custom navwalker
require get_template_directory() . '/inc/bs4Navwalker.php';
require get_template_directory() . '/inc/wpb_widget.php';

// Register WordPress nav menu
register_nav_menu('top', 'Top menu');

add_theme_support( 'custom-logo', array(
    'header-text' => array( 'Site Logo', 'Selecione uma logo para ser usada na barra de navegação' ),
) );

//Adiciona suporte a Post Thumbnails no Tema
add_theme_support( 'post-thumbnails' );

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Footer Widgets',
        'id'   => 'widgets',
        'description'   => 'Widgets do Rodapé',
        'before_widget' => '<div class="col-md-6 mt-auto mb-auto">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ));
}

// Register and load the widget
function wpb_load_widget() {
    register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

function excerpt( $limit ) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    } else {
        $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
    return $excerpt;
}

function content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
    } else {
        $content = implode(" ",$content);
    }
    $content = preg_replace('/[.+]/','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

add_action('init', 'custom_rewrite_basic');
function custom_rewrite_basic() {
    global $wp_post_types;
    foreach ($wp_post_types as $wp_post_type) {
        if ($wp_post_type->_builtin) continue;
        if (!$wp_post_type->has_archive && isset($wp_post_type->rewrite) && isset($wp_post_type->rewrite['with_front']) && !$wp_post_type->rewrite['with_front']) {
            $slug = (isset($wp_post_type->rewrite['slug']) ? $wp_post_type->rewrite['slug'] : $wp_post_type->name);
            $page = get_page_by_slug($slug);
            if ($page) add_rewrite_rule('^' .$slug .'/page/([0-9]+)/?', 'index.php?page_id=' .$page->ID .'&paged=$matches[1]', 'top');
        }
    }
}

function get_page_by_slug($page_slug, $output = OBJECT, $post_type = 'page' ) {
    global $wpdb;
    $page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s AND post_status = 'publish'", $page_slug, $post_type ) );
    return ($page ? get_post($page, $output) : NULL);
}


/** -------------------------------------------------------------
 * Adicinando um single difrente para cada categoria
-------------------------------------------------------------- */

add_filter('single_template', 'create_single_template');


function create_single_template( $template ) {
	global $post;

	$categories = get_the_category();
	// caso não tenhamos categoria retornamos o template default.
	if ( ! $categories )
		return $template;

	//resgatando o post type
	$post_type = get_post_type( $post->ID );

	$templates = array();

	foreach ( $categories as $category ) {
		// adicionando templates por id e slug
		$templates[] = "single-{$post_type}-{$category->slug}.php";
		$templates[] = "single-{$post_type}-{$category->term_id}.php";
	}

	// adicionando os templates padrões
	$templates[] = "single-{$post_type}.php";
	$templates[] = 'single.php';
	$templates[] = 'singular.php';
	$templates[] = 'index.php';

	return locate_template( $templates );
}

/** -------------------------------------------------------------
  * Personalizando a página de Login
-------------------------------------------------------------- */

function custom_login_css() {
	echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/css/login.css"/>';
}
add_action('login_head', 'custom_login_css');

function my_login_logo_url() {
	return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
	return 'Voltar para Home';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );


/**--------------------------------------------------------------
 * Redirecionar usuário para o site após login bem sucedido.
-------------------------------------------------------------- */

function my_login_redirect( $redirect_to, $request, $user ) {
	global $user;
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		if ( in_array( 'administrator', $user->roles ) ) {
			return $redirect_to;
		} else if ( in_array( 'subscriber', $user->roles ) ) {
			return get_bloginfo( 'url' ).'/';
		} else {
			return get_bloginfo( 'url' ).'/';
		}
	} else {
		return $redirect_to;
	}
}
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

/** -------------------------------------------------------------
 * Componente Banner
-------------------------------------------------------------- */

require get_parent_theme_file_path('/atelie/components/banner.php');


<?php
get_header();
?>

<main role="main" class="container">
	<?php if ( have_posts() ) :
		while ( have_posts() ) :
			the_post(); ?>
            <div class="row">
                <div class="col-12">
                    <h1 class="text-center">
						<?php the_title(); ?>
                    </h1>
                </div>
                <div class="col-8 offset-2 text-center">
                    <img style="max-width:100%!important;" src="<? echo get_the_post_thumbnail_url(); ?>"
                         alt="<?php get_the_post_thumbnail_caption(); ?>">
                </div>
                <div class="col-12">
					<?php the_content(); ?>
                </div>
            </div>
		<?php
		endwhile;
	endif; ?>
</main>

<?php
get_footer();
?>


<?php

$index=0;
if ( have_posts() ) : while (have_posts()) : the_post();
    if(($index%3.0)==0):
        ?>

        <div class="card-deck p-0 p-lg-2 col-12 mt-3">
    <?php endif; // end if ?>
    <div class="card col-12 p-0 col-lg-4 shadow-lg border-lg">
        <?php if(has_post_thumbnail()):?>
            <img class="card-img-top" src="<?php echo get_the_post_thumbnail_url()?>" alt="<?php get_the_post_thumbnail_caption()?>">
        <?php else: ?>
            <img class="card-img-top" src="<?php echo get_template_directory_uri()."/imgs/noimage.png" ?>" alt="<?php get_the_post_thumbnail_caption()?>">
        <?php endif; ?>
        <div class="card-body">
            <h5 class="card-title"><?php the_title() ?></h5>
            <p class="card-text"><?php echo excerpt(15) ?></p>
        </div>
        <div class="card-footer">
            <small class="text-muted mt-auto"><?php echo get_the_date('j')." de ".get_the_date('F')." de ".get_the_date('Y') ?></small>
            <a href="<?php the_permalink()?>" class="btn btn-primary float-right">Veja Mais</a>
        </div>
    </div>

    <?php
    $index+=1;
    if(($index%3.0)==0):?>
        </div>
    <?php endif; // end if ?>
<?php
endwhile; // end while?>
    </div>
<?php
else:
    echo '<div class="row">';
    echo '<h1>Desculpe não há nenhum post!</h1>';
    echo '</div>';
endif; // end if?>
        <!-- Footer -->
        <footer class="footer page-footer font-small teal pt-4">

            <!-- Footer Text -->
            <div class="container-fluid text-center text-md-left">

                <!-- Grid row -->
                <div class="row">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Widgets') ) : ?>
                    <?php endif; ?>
                </div>
                <!-- Grid row -->

            </div>
            <!-- Footer Text -->

            <!-- Copyright -->
            <a class="celulaweb" href="https://celulaweb.com.br" target="_blank">
                <div class="text-center bg-secondary p-2">
                    <span class="footer-copyright">DESENVOLVIDO POR </span>
                    <img src="<?php echo get_template_directory_uri()?>/imgs/logo_celula.png" style="height: 40px">
                </div>
            </a>
            <!-- Copyright -->

        </footer>
        <!-- Footer -->
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="<?php echo get_template_directory_uri()?>/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
        <?php wp_footer(); ?>
        
        <script>
            $(document).ready(function(){
                window.onscroll = function() {scrollFunction()};

                function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("goTop").style.display = "block";
                } else {
                    document.getElementById("goTop").style.display = "none";
                }
                }
            });

            $("#goTop").click(function(){
                $("html, body").animate({ scrollTop: 0 }, 600);
                return false;
            });

            $(".navbar-toggler").click(function(){
                $("#navbarCollapse").toggleClass("show-sidebar");
                $(".overlay").toggleClass("show-sidebar");
            });
        </script>
    </body>
</html>
<?php
// Register Custom Post Type
function custom_post_type() {

    $labels = array(
        'name'                  => _x( 'Banner', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Imagem', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Banners', 'text_domain' ),
        'add_new'               => __( 'Adicionar novo Banner', 'text_domain' ),
        'new_item'              => __( 'Adicionar novo Banner', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Imagem', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-gallery',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => false,
        'capability_type'       => 'page',
    );
    register_post_type( 'Banner', $args );

}
add_action( 'init', 'custom_post_type', 0 );
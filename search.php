<?php get_header(); ?>

    <main role="main" class="container">

    <?php
    $s=get_search_query();
    $args = array(
        's' =>$s
    );
    $index=0;
    // The Query
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) { ?>
        <div class="row">
            <div class="col-12">
                <h2 class="mt-4 mb-4 text-center" style='font-weight:bold;color:#000'>Resultados da pesquisa para: <?php echo get_query_var('s') ?> </h2>
            </div>
        </div>
        <?php while ($the_query->have_posts()) {
            $the_query->the_post();
            if(($index%3.0)==0):
            ?>

            <div class="card-deck p-2 col-12">
            <?php endif; // end if ?>
            <div class="card col-12 p-0 col-lg-4"  style="box-shadow: 3px 3px 5px grey;">
                <?php if(has_post_thumbnail()):?>
                    <img class="card-img-top" src="<?php echo get_the_post_thumbnail_url()?>" alt="<?php get_the_post_thumbnail_caption()?>">
                <?php else: ?>
                    <img class="card-img-top" src="<?php echo get_template_directory_uri()."/imgs/noimage.png" ?>" alt="<?php get_the_post_thumbnail_caption()?>">
                <?php endif; ?>
                <div class="card-body">
                    <h5 class="card-title"><?php the_title() ?></h5>
                    <p class="card-text"><?php echo excerpt(15) ?></p>
                </div>
                <div class="card-footer">
                    <small class="text-muted mt-auto"><?php echo get_the_date('j')." de ".get_the_date('F')." de ".get_the_date('Y') ?></small>
                    <a href="<?php the_permalink()?>" class="btn btn-primary float-right">Veja Mais</a>
                </div>
            </div>

            <?php
            $index+=1;
            if(($index%3.0)==0):?>
                </div>
            <?php endif; // end if ?>
        <?php
        }
    }else{
        ?>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="mt-4 text-center" style='font-weight:bold;color:#000'>Nothing Found</h2>
                    <div class="alert alert-info">
                        <p>Nada encontrado para sua pesquisa. Tente com palavras chaves diferentes</p>
                    </div>
                </div>
            </div>
        </div>
<?php
    }
?>
    </main>
<?php get_footer(); ?>
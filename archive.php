<?php
    get_header();
?>
    <main role="main" class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="page-title text-center mt-2 mb-3"> <?php echo single_cat_title( '', false ); ?></h2>
            </div>
        </div>
        <div class="gray-background">
            <?php get_template_part('loop'); ?>
        </div>
        <div class="row mt-3">
            <div class="col-12 ">
                <?php if(get_previous_posts_link ()!=""): ?>
                    <div class="nav-next btn btn-outline-primary float-left ml-2 mr-2"><?php previous_posts_link( 'Anterior' ); ?></div>
                <?php
                    endif;
                    if(get_next_posts_link ()!=""):
                ?>
                    <div class="nav-previous btn btn-outline-primary float-right ml-2 mr-2 mr-lg-4"><?php next_posts_link ( 'Próxima' ); ?></div>
                <?php endif;?>
            </div>
        </div>
    </main>
<?php
get_footer();
?>
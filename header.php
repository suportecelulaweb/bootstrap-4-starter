<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="<?php echo get_template_directory_uri()?>/js/jquery-3.5.1.slim.min.js"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri()?>/style.css" >
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <title><?php  wp_title( '|', true, 'right' );  ?></title>

    <?php wp_head(); ?>

</head>
<body >
    <button onclick="topFunction()" id="goTop"><i class="fa fa-arrow-up" ></i> </button>
    <nav id="top" class="navbar navbar-expand-lg navbar-light bg-light" style="box-shadow: 1px 1px 10px grey;">
        <div class="container">
            <div class="d-flex flex-column align-items-start w-100">
                <div class="row align-items-center">
                    <a class="navbar-brand col-9 col-lg-5 p-0 m-0" href="<?php  echo get_bloginfo('url');?>">
                        <?php if ( function_exists( 'the_custom_logo' ) && get_theme_mod('custom_logo')) :
                            $custom_logo_id = get_theme_mod( 'custom_logo' );
	                        $custom_logo_url = wp_get_attachment_image_url( $custom_logo_id, 'full');
                            echo '<img class="custom-logo" src="'.$custom_logo_url.'" alt="'.get_bloginfo('name').'">';
//                        the_custom_logo();
                        else:
                            echo get_bloginfo('name');
                        endif; ?>
                    </a>
                    <form class="form-inline d-none col-4 offset-md-3 d-none d-lg-block" role="search" action="<?php echo home_url(); ?>" method="get">
                        <input class="form-control search" type="text" name="s" id="s" placeholder="Pesquisar..." autocomplete="off" spellcheck="false" value="" autocorrect="off" />
                        <button type="submit"  id="searchsubmit" class="btn btn-link text-dark"> <i class="fa fa-search fa-sm" aria-hidden="true"></i> </button>
                    </form>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <?php
                wp_nav_menu([
                    'menu'            => 'top',
                    'theme_location'  => 'top',
                    'container'       => 'div',
                    'container_id'    => 'navbarCollapse',
                    'container_class' => 'collapse navbar-collapse ml-lg-0 ml-3',
                    'menu_id'         => false,
                    'menu_class'      => 'navbar-nav',
                    'depth'           => 2,
                    'items_wrap'     => '<ul id="%1$s" class="%2$s side-nav">
                                            <li class="menu-item d-lg-none">
                                                <img class="float-left logo-menu" src="'.$custom_logo_url.'"/> 
                                                <div class="close" style="position: relative;">
                                                    <button class="navbar-toggler center-icon-close" type="button" data-toggle="collapse" data-target="#navbarCollapse"><i class="fas fa-times"></i></button>
                                                </div>
                                            </li>
                                            <li class="menu-item">
                                                <form class="d-flex d-lg-none row" role="search" action="'.home_url().'" method="get">
                                                    <input class="form-control search col-sm-10 form-w85" type="text" name="s" id="s" placeholder="Pesquisar..." autocomplete="off" spellcheck="false" value="" autocorrect="off" />
                                                    <button type="submit"  id="searchsubmit" class="btn btn-link text-dark icon-w10"> <i class="fa fa-search fa-sm" aria-hidden="true"></i> </button>
                                                </form>
                                            </li>
                                            %3$s
                                          </ul>',
                    'fallback_cb'     => 'bs4navwalker::fallback',
                    'walker'          => new bs4navwalker()
                ]);
                ?>
                <div class="overlay"></div>
            </div>
        </div>
    </nav>
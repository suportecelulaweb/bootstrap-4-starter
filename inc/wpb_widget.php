<?php
// Creating the widget
class wpb_widget extends WP_Widget {

    function __construct() {
        parent::__construct(

// Base ID of your widget
            'wpb_widget',

// Widget name will appear in UI
            __('WPBeginner Widget', 'wpb_widget_domain'),

// Widget description
            array( 'description' => __( 'Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain' ), )
        );
    }

// Creating widget front-end

    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $location = apply_filters( 'widget_title', $instance['location'] );
        $phone = apply_filters( 'widget_title', $instance['phone'] );

// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        echo '<div class="col-sm-12 col-lg-6">';
        if ( ! empty( $title ) ) {
            echo '<div class="row">';
            echo '<div class="col-12">';
            echo $args['before_title'] . $title . $args['after_title'];
            echo '</div>';
            echo '</div>';
        }
        if ( ! empty( $location ) ) {
            echo '<div class="row">';
            echo '<div class="col-12">';
            echo '<p class="text-center">';
            echo $location;
            echo '</p>';
            echo '</div>';
            echo '</div>';
        }
        if ( ! empty( $phone ) ) {
            echo '<div class="row">';
            echo '<div class="col-12">';
            echo '<p class="text-center">';
            echo $phone;
            echo '</p>';
            echo '</div>';
            echo '</div>';
        }
        echo '</div>';



// This is where you run the code and display the output
        echo $args['after_widget'];
    }

// Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php

        if ( isset( $instance[ 'location' ] ) ) {
            $location = $instance[ 'location' ];
        }
        else {
            $location = __( 'Rua Teste, Bairro, N°222 - Cidade - UF', 'wpb_widget_domain' );
        }
// Widget lozalização form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'location' ); ?>"><?php _e( 'Localização:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'location' ); ?>" name="<?php echo $this->get_field_name( 'location' ); ?>" type="text" value="<?php echo esc_attr( $location ); ?>" />
        </p>
        <?php

        if ( isset( $instance[ 'phone' ] ) ) {
            $phone = $instance[ 'phone' ];
        }
        else {
            $phone = __( '(37)9 9999-9999', 'wpb_widget_domain' );
        }
// Widget lozalização form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e( 'Localização:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" type="text" value="<?php echo esc_attr( $phone ); ?>" />
        </p>
        <?php

        if ( isset( $instance[ 'image' ] ) ) {
            $phone = $instance[ 'image' ];
        }
        else {
            $phone = __( 'logo.png', 'wpb_widget_domain' );
        }
// Widget lozalização form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'image' ); ?>"><?php _e( 'Logo do Rodapé:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'image' ); ?>" name="<?php echo $this->get_field_name( 'image' ); ?>" type="file"  accept="image/x-png,image/gif,image/jpeg" value="<?php echo esc_attr( $phone ); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['location'] = ( ! empty( $new_instance['location'] ) ) ? strip_tags( $new_instance['location'] ) : '';
        $instance['phone'] = ( ! empty( $new_instance['phone'] ) ) ? strip_tags( $new_instance['phone'] ) : '';
        $instance['image'] = ( ! empty( $new_instance['image'] ) ) ? strip_tags( $new_instance['image'] ) : '';
        return $instance;
    }
} // Class wpb_widget ends here
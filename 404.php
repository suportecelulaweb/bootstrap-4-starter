<?php

/**
 * The template for displaying 404 pages (Not Found)
 **/

get_header(); ?>

    <main role="main" class="container mt-4">
        <div class="row">
            <div class="col-12">
                <h2 class="page-title text-center">This page, not found in this server!</h2>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="mt-4 text-center"></h2>
                    <div class="col-sm-12 col-lg-6 offset-sm-0 offset-lg-3">
                        <?php if ( function_exists( 'the_custom_logo' ) && get_theme_mod('custom_logo')) :
                            $custom_logo_id = get_theme_mod( 'custom_logo' );
	                        $custom_logo_url = wp_get_attachment_image_url( $custom_logo_id, 'full');
                            echo '<img class="custom-logo" src="'.$custom_logo_url.'" alt="'.get_bloginfo('name').'">';
                        else:
                            echo '<h2 class="text-center mt-2">'.get_bloginfo('name').'</h2>';
                        endif; ?>
                    </div>
                    <div class="alert alert-danger mt-4" role="alert">
                        Esta Pagina Não foi Encontrada em Nosso Servidor
                    </div>
                </div><!-- .page-content -->
            </div><!-- .page-wrapper -->
        </div>
    </main>

<?php get_footer();